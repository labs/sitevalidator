NAME
    sitescan - check website for errors

SYNOPSIS
    sitescan [-v|-s] [-c|-n] [-t sec] [-q|-a] url
    sitescan -h

DESCRIPTION
    scansite takes a homepage url of a website we want to check
    for errors. Then it opens Firefox browser and loads every page from
    this site (by following the links from one page to another) one
    after other. During this process it logs every error as "Not Found"
    errors, errors in javascript and html and css syntax errors.
    It also verifies existence of external resources to which links
    on this site point.
    
    Given url can contain path other than simple "/". In that case
    only webpages in this directory are considered part
    of the site. (e.g. if you specify http://example.com/path/ then
    http://example.com/path/page is considered part of the site while
    http://example.com/otherpath is not).
    
    In the beginning, a queue of urls is created which will hold all the
    found urls which have not been scanned yet. Input parameter url is
    put to this queue and followning cycle begins:
    One url is taken from the queue and HEAD request to this url is issued
    to see if the resource exists and if is html file (based upon sent
    content-type header) which means that it is a web page. If it is
    a web page from our site, we load it by Firefox to be able to verify
    not only the static part of the page, but also to let any
    javascript/ajax parts get executed. Then we find all <a> tags
    with links to other pages and we add their url to the queue. Next step
    is to pass all html and css files to the validator. Finally we report
    all found errors.
    
    Output of this program is a list of found issues categorized by their
    severity and grouped by resources where they were found.

OPTIONS

    -c  Valitate content of html and css files using w3c validators
        and show all found errors. To be able to validate html conent
        you must specify in the config file which validator is to be used.
        This option negates -n
    
    -h  Show readme and quit.
    
    -n  Do not validate html and css content.
    
    -s  Do not check content or HTTP headers of loaded external
        resources (from other domains).
        This option negates -v
    
    -t sec
        Spefifies how many seconds the browser should wait after a page
        has been loaded to let javascript perform asynchronous operations.
        Sec must be a positive integer. If none of the scanned pages use
        onload hooks, feel free to set it to 0.
        Default value is 2 seconds.
    
    -v  Check content and HTTP headers of external resources (resources
        loaded from other domains) as third party javascript libraries,
        css files, web pages put in an iframe and so on.
        This option negates -v
    
    url Absolute URL of the page to be scanned. It must be in the form
        http(s)://(subdomain.).domain.tld)/(path/) As a domain name
        it takes either FQDN or a local hostname (e.g. "localhost").
        In the former case any links to local names are deemed errors,
        whereas in the latter they are not.

FILES
    ~/.java/sitevalidator.xml
    contains configuration values for the application in the key value form
    validateContent     possible values: "yes" or "no",
                        same meaning as options -c and -n respectively
    useValidator        possible values: "local" or "remote"
                        which HTML validator we use
    localHtmlValidator  if you have a local installation of a html validator,
                        put here the path to its cgi script check
    remoteHtmlValidator url of remote validator script, normally you can
                        keep default value http://validator.w3.org/check
    validatorEncoding   encoding of the output from the HTML validator
    cssFallbackEncoding default encoding if a css file doesn't specify one
    reportAllErrors     possible values "yes" or "no"linked
                        same meaning as options -v and -s respectively
    loadWait            possible values: positive integers

REPORTING BUGS
    You can report sitescan bugs on the project's bug tracking page
    https://gitlab.labs.nic.cz/labs/sitechecker/issues or send
    an e-mail to the developer <pavol.otto@nic.cz>

COPYRIGHT
    Copyright © 2013 CZ.NIC, z.s.p.o.
    Released under Apache License 2 http://www.apache.org/licenses/LICENSE-2.0.txt
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.
