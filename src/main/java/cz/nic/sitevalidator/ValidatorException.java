/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.nic.sitevalidator;

/**
 *
 * @author palko
 */
public class ValidatorException extends RuntimeException {
    public ValidatorException(String s) {
        super(s);
    }
}
