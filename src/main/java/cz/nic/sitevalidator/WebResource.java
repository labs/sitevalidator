package cz.nic.sitevalidator;

import cz.nic.sitevalidator.util.WebResourceCycleException;
import cz.nic.sitevalidator.util.CharsetException;
import cz.nic.sitevalidator.util.ErrorItem;
import cz.nic.sitevalidator.util.MimeTypeStringDecoder;
import java.io.IOException;

import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import javax.net.ssl.SSLException;

import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.core.har.HarNameValuePair;
import net.lightbody.bmp.core.har.HarResponse;
import net.lightbody.bmp.proxy.util.Base64;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.css.css.DocumentParser;
import org.w3c.css.parser.CssError;
import org.w3c.css.parser.CssParseException;
import org.w3c.css.util.ApplContext;
import org.w3c.css.util.CssVersion;
import org.w3c.css.util.InvalidParamException;
import org.w3c.www.mime.MimeType;
import org.w3c.www.mime.MimeTypeFormatException;

/**
 *
 * @author palko
 */
public class WebResource extends WebObject {
    // appl context is used for css validation
    // as it never changes, we initialize it here and make it static
    private static final ApplContext AC;
    static {
        AC = new ApplContext("en");
        AC.setCssVersionAndProfile(CssVersion.getDefault().toString()); // css3
        AC.setMedium("all");
        // vendorExtensionsAsWarnings:
        // false - report properties starting with - (like -moz-*, -webkit-*,...)
        // as errors; true - report them as warnings
        // (and the behaviour now is to discard the warnings)
        AC.setTreatVendorExtensionsAsWarnings(true);
        AC.setFollowlinks(false);
    }
    
    private String mime = "";
    private boolean html200Flag = false;
    // resources on which there are links to this resource
    final Set<WebResource> referencingResources = new HashSet<>();
    private final boolean validate;
    
    private boolean setValidate() {
        boolean allErrorsConf = ApplSettings.getSettings().getProperty("reportAllErrors").equalsIgnoreCase("yes");
        boolean sameHost = scanner.getRootURL().getHost().equals(this.url.getHost());
        return sameHost || allErrorsConf;
    }
    
    public WebResource(URL url, SiteChecker scanner) {
        super(url, scanner);
        validate = setValidate();
    }
    
    public WebResource(String url, SiteChecker scanner) {
        super(url, scanner);
        validate = setValidate();
    }
    
    /**
     * Do HEAD request and check response code, content-type and if any error
     * occurred, add it to this.errors. Return null if we should validate this
     * resource by selenium (in case of application/xhtml+xml, text/css, text/html).
     * Or return a string with redirect-to address (may be empty string).
     * 
     * @return Return null if HTTP_OK and content-type is html or css, or return a redirect-to string (or empty string).
     */
    public String headCheck() {
        HttpURLConnection.setFollowRedirects(false);
        try {
            HttpURLConnection conn =
                    (HttpURLConnection) this.url.openConnection();
            conn.setRequestMethod("HEAD");
            conn.setRequestProperty("Connection", "close");
            int respCode = conn.getResponseCode();
            if(respCode == HttpURLConnection.HTTP_OK) {
                mime = conn.getContentType();
            }
            else if (respCode == HttpURLConnection.HTTP_MOVED_PERM
                || respCode == HttpURLConnection.HTTP_MOVED_TEMP
                || respCode == HttpURLConnection.HTTP_SEE_OTHER
                || respCode == 307
                || respCode == 308) {
                String location = conn.getHeaderField("location");
                if(location == null || location.isEmpty()) {
                    add(new ErrorItem("Redirect response has not provided "
                        + "Location header.", Level.SEVERE));
                }
                else {
                    try {
                        String redirectLocation = new URL(url, location).toString();
                        if(!scanner.isValidURL(redirectLocation))
                            throw new MalformedURLException();
                        return redirectLocation;
                    }
                    catch(MalformedURLException e) {
                        add(new ErrorItem("Invalid Location url " + location,
                                Level.SEVERE));
                    }
                    
                }
                
                return "";
            }
            else {
                add(new ErrorItem(String.valueOf(conn.getResponseCode()) + " ("
                        + conn.getResponseMessage() + ") HTTP response",
                        Level.SEVERE));
                return "";
            }
        }
        catch(UnknownHostException e) {
            add(new ErrorItem("Could not resolve address."
                    + " Probably this domain doesn't exist.", Level.SEVERE));
            return "";
        }
        catch(SSLException e) {
            add(new ErrorItem("SSL error: " + e.getMessage() + " Try running "
                    + "this script with option -Djsse.enableSNIExtension=false "
                    + "Skipping validation of this resource."));
            return "";
        }
        catch(IOException e) {
            add(new ErrorItem("Error during doing HEAD request,"
                    + " skipping validation of this resource.", Level.SEVERE));
            return "";
        }
        
        if(mime == null || mime.isEmpty()) {
            add("Content-type HTTP header is missing. According"
                    + " to RFC 2616 it should define it (if not specified,"
                    + " defaults to application/octet-stream)");
            return "";
        }
        
        MimeType contentType;
        try {
            contentType = new MimeType(mime);
        }
        catch(MimeTypeFormatException e) {
            add("Cannot parse incorrectly formatted Content-type HTTP header + ("
                    + mime + ")");
            return "";
        }
        
        if(contentType.match(MimeType.APPLICATION_XHTML_XML) > 0
                || contentType.match(MimeType.TEXT_HTML) > 0) {
            html200Flag = true;
            return null;
        }
        else if(contentType.match(MimeType.TEXT_CSS) > 0) {
            return null;
        }
        else
            return "";
    }
    
    /**
     * Check this resource for errors, detect. Entry must correspond
     * to this resource. While checking if found that this resource
     * redirects to another, return new Location, otherwise return null.
     * 
     * @param entry HarEntry corresponding to this resource.
     * @return Address to which this redirect or null if not 30x redirect.
     */
    public String checkResponse(HarEntry entry) {
        HarResponse response = entry.getResponse();
        
        /* response
         * 200 - check content
         * 304 - silently ignore (resource is not modified)
         * 404 - if it is /favicon.ico, report it only as info
         */
        if(response.getStatus() == -998) {
            add(new ErrorItem("Could not resolve address."
                    + " Probably this domain doesn't exist.", Level.SEVERE));
        }
        else if(response.getStatus() < 200) {
            add(new ErrorItem("Strange HTTP status " + response.getStatus()
                    + "(" + response.getStatusText()+ ")"));
        }
        else if(response.getStatus() == 200) {
            if(validate)
                checkContent(entry);
        }
        else if(response.getStatus() < 300) {
            add(new ErrorItem("HTTP status code " + response.getStatus()
                    + " (Success) is not usual. You should check it.", Level.INFO));
        }
        else if(response.getStatus() == 301
                || response.getStatus() == 302
                || response.getStatus() == 303
                || response.getStatus() == 307
                || response.getStatus() == 308) {
            // add redirect to the table
            String location = null;
            for(HarNameValuePair header:response.getHeaders()) {
                if(header.getName().equalsIgnoreCase("location")) {
                    location = header.getValue();
                    break; // no reason to continue
                }
            }
            if(location != null && !location.isEmpty()) {
                try {
                    String redirectLocation = new URL(url, location).toString();
                    if(!scanner.isValidURL(redirectLocation))
                        throw new MalformedURLException();
                    // add referencingresource to the corresponding resource
                    if(scanner.hasResource(redirectLocation)) {
                        return redirectLocation;
                    }
                    else {
                        add(new ErrorItem("Resource " + redirectLocation +
                                " to which this redirects hasn't been loaded. (this "
                                + "is usually caused by too many chained redirects)",
                                Level.SEVERE));
                    }
                }
                catch(MalformedURLException e) {
                    add(new ErrorItem("Invalid Location url " + location,
                            Level.SEVERE));
                }
            }
            else {
                add(new ErrorItem("Redirect response has not provided "
                        + "Location header.", Level.SEVERE));
            }
        }
        else if(response.getStatus() == 404 && url.getPath().equals("/favicon.ico")) {
            add(new ErrorItem("Your site doesn't have /favicon.ico in the"
                    + " root directory. Consider adding one.", Level.INFO));
        }
        else if(response.getStatus() != 304) { // errors 300, 305, 306, >=309
            add(new ErrorItem(String.valueOf(response.getStatus()) + " "
                    + response.getStatusText() + " HTTP response", Level.SEVERE));
        }
            
        // message body is not allowed in case of 1xx, 204, 304 
        if(response.getBodySize() != 0 && (response.getStatus() < 200 ||
                response.getStatus() == 204 || response.getStatus() == 304)) {
            add(new ErrorItem(response.getStatus() + " response"
                    + " is not allowed to provide message body (see RFC2616 "
                    + "http://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.3).",
                    Level.SEVERE));
        }
        
        return null;
    }
    
    /**
     * Tries to decode the content using provided charset and
     * in case of html or css document validates the content.
     * @param entry HarEntry of this resource
     */
    private void checkContent(HarEntry entry) {
        mime = entry.getResponse().getContent().getMimeType();
        MimeType contenttype;
        
        if(mime == null) {
            add("Content-type HTTP header is missing. According"
                    + " to RFC 2616 it should define it (if not specified,"
                    + " defaults to application/octet-stream)");
            return;
        }
        
        try {
            contenttype = new MimeType(mime);
        }
        catch(MimeTypeFormatException e) {
            add("Cannot parse incorrectly formatted Content-type HTTP header + ("
                    + mime + ")");
            return;
        }
        
        if(contenttype.getType().equals("text")) {
            MimeTypeStringDecoder decoder;
            byte[] rawContent = Base64.base64ToByteArray(
                    entry.getResponse().getContent().getText());
            Charset charset;
            
            try {
                decoder = new MimeTypeStringDecoder(contenttype);
                charset = decoder.detectCharset(rawContent);
            }
            catch(CharsetException e) {
                add(new ErrorItem(e.toString(), Level.SEVERE));
                return;
            }
            
            switch(contenttype.getSubtype()) {
                case "javascript":
                case "ecmascript":
                    
                    add(String.format("Content-type text/%1$s is obsolete "
                            + "according to RFC4329, use application/%1$s instead.",
                            contenttype.getSubtype()));
                    break;
                case "css":
                    String body;
                    try {
                        body = decoder.decodeByteArray(rawContent,
                                charset != null ? charset : Charset.forName(
                                ApplSettings.getSettings().getProperty("cssFallbackEncoding")));
                    }
                    catch(CharacterCodingException e) {
                        // could not decode
                        if(charset != null)
                            add(new ErrorItem("Could not decode content, message"
                                    + " body contains invalid characters. Check "
                                    + "if provided charset " + charset.toString()
                                    + " is correct.", Level.SEVERE));
                        else
                            add(new ErrorItem("No charset specified neither in "
                                    + "content-type header nor in @charset",
                                    Level.SEVERE));
                        return;
                    }
                    try {
                        if(ApplSettings.getSettings().getProperty(
                                "validateContent").equalsIgnoreCase("yes"))
                            verifyCSS(body);
                    }
                    catch(Exception e) {
                        // log excepion to stderr
                        add(new ErrorItem("Skipping validation of this document "
                                + "caused by an error during validation.", Level.INFO));
                    }
                    break;
                case "html":
                    // see if there is a css snippet in the document and validate it
                    try {
                        verifyHTML(rawContent);
                    }
                    catch(Exception e) {
                        // log excepion to stderr
                        add(new ErrorItem("Skipping validation of this document "
                                + "caused by an error during validation.", Level.INFO));
                    }
                    break;
            }
        }
        else if(contenttype.getType().equals("application")) {
            switch(contenttype.getSubtype()) {
                case "x-javascript":
                    add("You should use application/javascript as content-type "
                            + "instead of application/x-javascript.");
                    break;
            }
        }
        // else it it an image, video... that we do not verify
    }
    
    /**
     * Checks input css document for validation errors.
     * It uses w3c validator (from http://jigsaw.w3.org/css-validator/).
     * Checks only the provided document itself, does not follow {@literal @}include.
     * @param content css document to verify
     */
    private void verifyCSS(String content)
            throws Exception {
        StringReader inputCSS = new StringReader(content);
        
        DocumentParser parser = new DocumentParser(AC, inputCSS,
                url.toString(), MimeType.TEXT_CSS);
        
        for(CssError error: parser.getStyleSheet().getErrors().getErrors()) {
            String errorText;
            Throwable exception = error.getException();
            if(exception instanceof CssParseException) {
                CssParseException cssParseEx = (CssParseException) exception;
                StringBuilder errorTextBuilder = new StringBuilder();
                String msg = cssParseEx.getMessage();
                if(msg != null)
                    errorTextBuilder.append(msg);
                else if(cssParseEx.getSkippedString() != null) {
                    errorTextBuilder.append("Parse error: ").append(
                            cssParseEx.getSkippedString());
                }

                String context = StringUtils.join(cssParseEx.getContexts(), ", ");
                if(context != null) {
                    errorTextBuilder.append(" (").append(context);
                    if(cssParseEx.getProperty() != null)
                        errorTextBuilder.append(", property ").append(
                                cssParseEx.getProperty());
                    errorTextBuilder.append(")");
                }
                errorText = errorTextBuilder.toString();
            }
            else if(exception instanceof InvalidParamException) {
                InvalidParamException invalidParamEx = (InvalidParamException) exception;
                String msg = invalidParamEx.getMessage();
                if(msg != null)
                    errorText = invalidParamEx.getMessage();
                else
                    errorText = "unknown error";
            }
            else {
                errorText = "unknown error of type " + exception.getClass().getName();
            }

            add(new ErrorItem(errorText, Level.SEVERE,
                    error.getLine(), ErrorItem.UNDEFINED));
        }
    }
    
    private void verifyHTML(byte[] contentBody) throws Exception {
        final String validatorHeaderStatus = "X-W3C-Validator-Status";
        
        ApplSettings as = ApplSettings.getSettings();
        StringWriter validatorOutput = new StringWriter();
        String validatorOutputString;
        String status;
        
        // create body of the message
        MultipartEntity body = new MultipartEntity();
        body.addPart("output", new StringBody("json"));
        body.addPart("uploaded_file", new ByteArrayBody(contentBody, mime, "file"));
        body.addPart("charset", new StringBody("(detect automatically)"));
        body.addPart("doctype", new StringBody("Inline"));
        body.addPart("group", new StringBody("0"));

        // now pass it to the cgi script or to http interface of the validator
        if(as.getProperty("useValidator").equals("local")) {
            // use local instance of the validator (call a cgi script)
            Process validator = Runtime.getRuntime().exec(
                    as.getProperty("localHtmlValidator"),
                     new String[]{"CONTENT_LENGTH=" + body.getContentLength(),
                    "REQUEST_METHOD=POST",
                    "CONTENT_TYPE=" + body.getContentType().getValue()});
            // try it without a special thread for reading output
            body.writeTo(validator.getOutputStream());
            validator.getOutputStream().close();
            IOUtils.copy(validator.getInputStream(), validatorOutput,
                    as.getProperty("validatorEncoding"));
            StringWriter errorOutput = new StringWriter();
            
            IOUtils.copy(validator.getErrorStream(), new OutputStreamWriter(System.out),
                    as.getProperty("validatorEncoding"));
            
            String[] split = validatorOutput.toString().split("(\r\r)|(\n\n)|(\r\n\r\n)", 2);
            if(split.length != 2 || split[0].isEmpty() || split[1].isEmpty()) {
                throw new ValidatorException("No header in the response.");
            }
            
            validatorOutputString = split[1];
            int headerPos = split[0].indexOf(validatorHeaderStatus + ":");
            if(headerPos != -1) {
                status = split[0].substring(headerPos
                        + validatorHeaderStatus.length() + 1).split("\r|\n", 2)[0];
            }
            else {
                throw new ValidatorException("No http header " + validatorHeaderStatus + ".");
            }
        }
        else {
            // use validator over http
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(as.getProperty("remoteHtmlValidator"));
            httpPost.setEntity(body);
            HttpResponse response = httpclient.execute(httpPost);
            status = response.getFirstHeader(validatorHeaderStatus).getValue().trim();
            IOUtils.copy(response.getEntity().getContent(), validatorOutput,
                    as.getProperty("validatorEncoding"));
            validatorOutputString = validatorOutput.toString();
        }
        
        JSONArray validatorErrors = new JSONObject(validatorOutputString).getJSONArray("messages");
        for(int x = 0; x < validatorErrors.length(); x++) {
            JSONObject jError = validatorErrors.getJSONObject(x);
            Level jLevel;
            switch(jError.getString("type")) {
                case "error": jLevel = Level.SEVERE; break;
                case "warning": jLevel = Level.WARNING; break;
                case "info": jLevel = Level.INFO; break;
                default: jLevel = Level.ALL; // the least important
            }
            ErrorItem error = new ErrorItem(jError.getString("message"),jLevel);
            try {
                error.setLine(jError.getInt("lastLine"));
                error.setColumn(jError.getInt("lastColumn"));
            }
            catch(JSONException e) {
                // This exception is raised always when lastLine or lastColumn
                // are not set. Just ignore it.
            }
            add(error);
        }
    }
    
    // methods for adding and iterating over WebResources which points to this resource
    public boolean addReference(WebResource linkedFrom) {
        return referencingResources.add(linkedFrom);
    }
    
    public Set<WebResource> getReferences() {
        return referencingResources;
    }
    
    // if this resource is a existing webpage (html status 200 and content is html)
    public boolean isHtml200() {
        return html200Flag;
    }
    
    public String reportErrors() {
        StringBuilder out = new StringBuilder();
        // while(!res.html200Flag)
        for(ErrorItem error: this) {
            String level;
            if(error.getLevel().equals(Level.SEVERE)) {
                level = "ERROR";
            }
            else {
                level = error.getLevel().toString();
            }
            out.append(level);
            if(error.getLine() != -1) {
                out.append(" line ").append(error.getLine());
            }
            if(error.getColumn() != -1) {
                out.append(" column ").append(error.getColumn());
            }
            out.append(": ").append(error.getMessage());
            out.append(System.lineSeparator());
        }
        return out.toString();
    }
    
    private String reportLinkingResources(int indent, WebResource start, boolean first) {
        // detect cyclic link and throw an Exception
        if(!first && this == start) {
            throw new WebResourceCycleException("Cycle detected when scanning "
                    + this.toString());
        }
        StringBuilder out = new StringBuilder();
        out.append(StringUtils.repeat("  ", indent));
        out.append(toString());
        out.append(System.lineSeparator());
        indent++;
        if(!html200Flag) {
            for(WebResource ref: getReferences()) {
                out.append(ref.reportLinkingResources(indent, start, false));
            }
        }
        return out.toString();
    }
    
    public String reportLinkingResources() {
        return reportLinkingResources(0, this, true);
    }
}
