/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.nic.sitevalidator.util;

import java.io.IOException;

/**
 * Checked exception for use in validator.
 * 
 * @author palko
 */
public class CharsetException extends IOException{
    public CharsetException() {
        super();
    }

    public CharsetException(String message) {
        super(message);
    }
}
