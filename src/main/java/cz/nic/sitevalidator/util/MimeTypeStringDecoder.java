/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.nic.sitevalidator.util;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.StandardCharsets;
import java.nio.charset.UnsupportedCharsetException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.w3c.www.mime.MimeType;
import org.w3c.www.mime.MimeTypeFormatException;

/**
 * Class for decoding a byte array into String. Uses charset found in
 * the mime type parameter charset. If not found, try to find information
 * about charset in the document itself (like {@literal @}charset in css,
 * html meta tag,...) or check for a byte order mark (UTF-8, UTF-16).
 * 
 * @author palko &lt;pavol.otto@nic.cz&gt;
 */
public class MimeTypeStringDecoder {
    private static enum BOM {
        UTF8, UTF16LE, UTF16BE, UTF32LE, UTF32BE, NO_BOM;
        
        /**
         * @return status if is valid BOM, or no bom detected
         */
        public boolean isBOM() {
            return !equals(NO_BOM);
        }
        
        /**
         * @return charset corresponding to bom detected or null if no bom
         */
        public Charset toCharset() {
            switch(this) {
                case UTF8:    return StandardCharsets.UTF_8;
                case UTF16BE: return StandardCharsets.UTF_16BE;
                case UTF16LE: return StandardCharsets.UTF_16LE;
                case UTF32BE: return Charset.forName("UTF-32BE");
                case UTF32LE: return Charset.forName("UTF-32LE");
                default:      return null;
            }
        }
        
        /**
         * Check if charset defined by bom is "child" of the charset charset.
         * By "child" we mean charset with endianness information,
         * so UTF-16LE and UTF-16BE are both "children" of "UTF-16".
         * And bom.isChild(bom.toCharset()), is always true unless bom is NO_BOM.
         */
        public boolean isChild(Charset charset) {
            if(charset.equals(toCharset()))
                return true;
            
            switch(this) {
                case UTF8:    return false;
                case UTF16BE: 
                case UTF16LE: return charset.equals(StandardCharsets.UTF_16);
                case UTF32BE: 
                case UTF32LE: return charset.equals(Charset.forName("UTF-32"));
                default:      return false;
            }
        }
        
        /**
         * Return number of bytes for this BOM
         */
        public int length() {
            switch(this) {
                case UTF8:    return 3;
                case UTF16BE: 
                case UTF16LE: return 2;
                case UTF32BE: 
                case UTF32LE: return 4;
                default:      return 0;
            }
        }
    }
    
    private MimeType mime;
    private final Charset charset;
    
    // private ErrorItem warning = null;
    
    /**
     * Create a decoder object for given mime type.
     * 
     * @param mimeType mime type of the document which this decoder will decode
     */
    public MimeTypeStringDecoder(MimeType mimeType) throws CharsetException {
        mime = mimeType;
        String charsetStr = mime.getParameterValue("charset");
        if(charsetStr != null && !charsetStr.isEmpty()) {
            try {
                charset = Charset.forName(charsetStr);
            }
            catch(UnsupportedCharsetException | IllegalCharsetNameException e) {
                throw new CharsetException("Charset defined in the mime type "
                        + charsetStr + " is not a valid charset.");
            }
        }
        else
            charset = null;
    }
    
    /**
     * Create a decoder object mime type given as a string (with optional
     * charset parameter).
     * 
     * @param mimeType mime type of the document which this decoder will decode
     */
    public MimeTypeStringDecoder(String mimeType)
            throws MimeTypeFormatException, CharsetException {
        this(new MimeType(mimeType));
    }
    
    private BOM detectBOM(byte[] document) {
        if((document.length >= 3) && (document[0] == (byte) 0xEF)
                && (document[1] == (byte) 0xBB) && (document[2] == (byte) 0xBF)) {
            return BOM.UTF8;
        }
        else if((document.length >= 2)
                && (document[0] == (byte) 0xFE) && (document[1] == (byte) 0xFF)) {
            return BOM.UTF16BE;
        }
        else if((document.length >= 2)
                && (document[0] == (byte) 0xFF) && (document[1] == (byte) 0xFE)) {
            return BOM.UTF16LE;
        }
        else if((document.length >= 4)
                && (document[0] == (byte) 0x00) && (document[1] == (byte) 0x00)
                && (document[2] == (byte) 0xFE) && (document[3] == (byte) 0xFF)) {
            return BOM.UTF32BE;
        }
        else if((document.length >= 4)
                && (document[0] == (byte) 0xFF) && (document[1] == (byte) 0xFE)
                && (document[2] == (byte) 0x00) && (document[3] == (byte) 0x00)) {
            return BOM.UTF32LE;
        }
        else {
            return BOM.NO_BOM;
        }
    }
    
    private static String matchCharsetDef(String regex, String doc, int flags) {
        Matcher matcher = Pattern.compile(regex, flags).matcher(doc);
        if(matcher.find()) {
            return matcher.group("charset");
        }
        else
            return null;
    }
    
    /**
     * Try to find out what charset is used by document. It may be provided by
     * the mime type, byte order mark at the beginning of the byte array or
     * by specific ways for given mime type (like {@literal @}charset rule in
     * a css file.
     * This method does not verify if 
     * 
     * @param document raw data of the document which is to be 
     * @return detected charset or null if document charset cannot be detected
     * @throws CharsetException 
     */
    public Charset detectCharset(byte[] document) throws CharsetException {
        // at first check for BOM
        BOM bom = detectBOM(document);
        
        // if bom found, return it, then use mimetype charset and if neither found, search in the document
        if(bom.isBOM()) {
            // if bom exists and does not match the mimetype charset, throw an exception
            if(charset != null && !charset.equals(bom.toCharset())
                    && !bom.isChild(charset)) {
                throw new CharsetException("Mime type charset " + charset.name() + " is not "
                    + "the same as found by decoding BOM (" + bom.toCharset().name() + ").");
            }
            
            return bom.toCharset();
        }
        if(charset != null) {
            return charset;
        }
        else {
            String decoded = new String(document, StandardCharsets.US_ASCII);
            String charsetTagRegex;
            String detectedCharset = null;
            if(mime.match(MimeType.TEXT_CSS) > 0) {
                // search for @charset at the beginning of the string
                charsetTagRegex = "^@charset\\s+([\"'])(?<charset>[^'\"]+)\\1;";
                detectedCharset = matchCharsetDef(charsetTagRegex, decoded, 0);
            }
            else if(mime.match(MimeType.TEXT_HTML) > 0) {
                // search for either <meta http-equiv="content-type">
                // or html5 tag <meta charset>
                /* TODO consider putting these two regexes into one long (maybe with lookahead) and consequently
                 * you will be able to take "matchCharsetDef" call out of this if statement */
                charsetTagRegex = "<meta\\s+http-equiv\\s*=\\s*([\"'])content-type\\1\\s+content\\s*=\\s*([\"'])[a-z]+/[a-z]+;\\s*charset=(?<charset>[^\"']+)\\2\\s*/?>"; 
                detectedCharset = matchCharsetDef(charsetTagRegex, decoded,
                        Pattern.CASE_INSENSITIVE);
                if(detectedCharset == null) {
                    charsetTagRegex = "<meta\\s+charset\\s*=\\s*([\"'])(?<charset>[^\"']+)\\1\\s*/?>";
                    detectedCharset = matchCharsetDef(charsetTagRegex, decoded,
                            Pattern.CASE_INSENSITIVE);
                }
            }
            else if(mime.match(MimeType.APPLICATION_XML) > 0 || mime.getSubtype().endsWith("+xml")) {
                // search for <?xml version="1.0" encoding="UTF-8"?>
                charsetTagRegex = "^<\\?xml\\s+version\\s*=\\s*([\"'])\\d+\\.\\d+\\1\\s+encoding\\s*=\\s*([\"'])(?<charset>[^\"']+)\\2";
                detectedCharset = matchCharsetDef(charsetTagRegex, decoded, 0);
            }
            
            if(detectedCharset != null) {
                try {
                    return Charset.forName(detectedCharset);
                }
                catch(UnsupportedCharsetException | IllegalCharsetNameException e) {
                    throw new CharsetException("Charset defined in the document "
                        + detectedCharset + " is not a valid charset.");
                }
            }
            else
                return null; // charset can not be detected
        }
    }
    
    /**
     * Decode byte array to string using charset charset. If an invalid byte for
     * given charset is found, which can not be decoded, throw
     * a CharacterCodingException exception.
     * 
     * @param input Raw data in byte array to decode into string. Can not be null.
     * @param charset Charset to use. Can not be null.
     * @return decoded string
     */
    public String decodeByteArray(byte[] rawInput, Charset charset)
            throws CharacterCodingException {
        // create decoder for given charset
        CharsetDecoder decoder = charset.newDecoder();
        decoder.onMalformedInput(CodingErrorAction.REPORT);
        
        // detect BOM
        BOM bom = detectBOM(rawInput);
        
        // create byte buffer 
        int dataWoBOMlength = rawInput.length - bom.length();
        ByteBuffer bb = ByteBuffer.allocate(dataWoBOMlength);
        bb.put(rawInput, bom.length(), dataWoBOMlength);
        bb.rewind();
        return decoder.decode(bb).toString();
    }
}
