/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.nic.sitevalidator.util;

/**
 * Runtime exception which is thrown in case of detected cycle
 * in WebResources links (as cyclic redirects) when none of WebResources
 * in the cycle has html200flag set.
 */
public class WebResourceCycleException extends RuntimeException {
    public WebResourceCycleException() {
        super();
    }
    
    public WebResourceCycleException(String msg) {
        super(msg);
    }
}
