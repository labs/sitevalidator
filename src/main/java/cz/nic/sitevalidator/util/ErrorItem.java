/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.nic.sitevalidator.util;

import java.util.logging.Level;

/**
 *
 * @author palko
 */
public class ErrorItem {
    public static final int UNDEFINED = -1;
    
    private String error_msg;
    private Level severity;
    private int line;
    private int column;
    
    public ErrorItem(String msg) {
        this(msg, Level.WARNING);
    }
    
    public ErrorItem(String msg, Level level) {
        this(msg, level, UNDEFINED, UNDEFINED);
    }
    
    public ErrorItem(String msg, Level level, int ln, int col) {
        error_msg = msg;
        severity = level;
        line = ln;
        column = col;
    }
    
    public String getMessage() {
        return error_msg;
    }
    
    public void setMessage(String msg) {
        this.error_msg = msg;
    }
    
    public Level getLevel() {
        return severity;
    }
    
    public void setLevel(Level level) {
        this.severity = level;
    }
    
    public int getLine() {
        return line;
    }
    
    public void setLine(int ln) {
        line = ln;
    }
    
    public int getColumn() {
        return column;
    }
    
    public void setColumn(int col) {
        column = col;
    }
}
