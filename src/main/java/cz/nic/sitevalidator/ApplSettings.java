/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.nic.sitevalidator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

/**
 * Singleton immutable settings class.
 * 
 * 
 * @author palko
 */
class ApplSettings {
    private static final ApplSettings instance = new ApplSettings();
    private static boolean lock = false;
    private Properties props;
    
    private ApplSettings() {
        InputStream fileStream;
        try {
            fileStream = new FileInputStream(new File(
                    System.getProperty("user.home"), ".java/sitevalidator.xml"));
        }
        catch(FileNotFoundException e) {
            throw new RuntimeException("Config file ~/.java/sitevalidator.xml doesn't exist.");
        }
        
        props = new Properties();
        try {
            props.loadFromXML(fileStream);
        }
        catch(IOException e) {
            throw new RuntimeException("Cannot read configuration from config.xml file.", e);
        }
    }
    
    /**
     * Take the command line arguments and sets the application
     * properties accordingly. It can be called only before getSettings
     * is called. After it call to this function raises a RuntimeException.
     * 
     * @param args command line args
     */
    public static void setCommandLineArgs(Map<String, String> args) {
        if(lock)
            throw new RuntimeException("Method called after call to the getSettings."
                    + "(you can set it only in the very beginning)");
        
        lock = true;
        
        for(Entry<String, String> entry: args.entrySet()) {
            instance.props.setProperty(entry.getKey(), entry.getValue());
        }
    }
    
    public static ApplSettings getSettings() {
        lock = true;
        return instance;
    }
    
    /**
     * Searches for the property with the specified key. Throws
     * an RuntimeException if the key is not found.
     * @throws RuntimeException 
     * @param key
     * @return Property as string.
     */
    public String getProperty(String key) {
        String value = props.getProperty(key);
        if(value == null) {
            throw new RuntimeException();
        }
        
        return value;
    }
}
