/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.nic.sitevalidator;

import java.text.MessageFormat;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 *
 * @author palko
 */
public class OneLineFormatter extends Formatter {
    @Override
    public String format(LogRecord record) {
        String leveltxt = "UNKNOWN LEVEL";
        // try {
            StringBuilder sb = new StringBuilder();
            Level level = record.getLevel();
            if (level.equals(Level.WARNING)) {
                leveltxt = "WARNING";
            } else if (level.equals(Level.SEVERE)) {
                leveltxt = "ERROR";
            } else if (level.equals(Level.INFO)) {
                leveltxt = "INFO";
            } else if (level.equals(Level.FINE) || level.equals(Level.FINER)
                       || level.equals(Level.FINEST)) {
                leveltxt = "DEBUG";
            } else if (level.equals(Level.CONFIG)) {
                leveltxt = "CONFIG";
            }

            sb.append(leveltxt).append(": ");
            
            if (record.getParameters() != null && record.getParameters().length > 0) {
                try {
                    sb.append(MessageFormat.format(record.getMessage(),
                                                   record.getParameters()));
                }
                catch(IllegalArgumentException e) {
                    sb.append("CRITICAL: cannot format folowing string \"");
                    sb.append(record.getMessage());
                    sb.append("\" (in class ");
                    sb.append(record.getSourceClassName()).append(")");
                }
                
                /* java.util.Formatter formatter = new java.util.Formatter(sb);
                formatter.format(record.getMessage(), record.getParameters());
                formatter.format("\n"); */
            } else {
                sb.append(record.getMessage());
            }
            
            sb.append("\n");
            return sb.toString();
        /* } catch (Exception e) {
            System.err.println("*******************************************************");
            System.err.println("There was a problem formatting a log statement:");
            e.printStackTrace();
            System.err.println("We will return the raw message and print any stack now");
            System.err.println("*******************************************************");

            if (record.getThrown() != null) {
                System.err.println("Root stack trace:");
                record.getThrown().printStackTrace();
                System.err.println("*******************************************************");
            }

            return record.getMessage() + "\n";
        } */
    }
}
