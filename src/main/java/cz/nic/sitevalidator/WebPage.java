/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.nic.sitevalidator;

import cz.nic.sitevalidator.util.ErrorItem;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.jsourcerer.webdriver.jserrorcollector.JavaScriptError;
import net.lightbody.bmp.core.har.HarEntry;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author palko
 */
public class WebPage extends WebResource {
    private FirefoxDriver driver;
    boolean loaded = false;
    private static final ApplSettings as = ApplSettings.getSettings();
    
    public WebPage(String url, SiteChecker scanner) {
        super(url, scanner);
    }
    
    public WebPage(URL url, SiteChecker scanner) {
        super(url, scanner);
    }
    
    public void load() {
        // obtain the lock for Firefox
        driver = scanner.getFirefox(this);
        
        scanner.proxyServer.newHar(toString());
        driver.get(toString());
        
        /* wait specified number of seconds according to the -t command line
         * parameter or config loadWait option */
        int seconds;
        try {
            seconds = Integer.parseInt(
                    WebPage.as.getProperty("loadWait"));
        }
        catch(NumberFormatException e) {
            seconds = 2;
        }
        
        try {
            Thread.sleep(seconds * 1000);
        }
        catch(InterruptedException e) {
            Logger.getLogger(WebPage.class.getCanonicalName()).log(Level.WARNING,
                    "Sleeping interrupted. (cz.nic.sitevalidator.WebPage)");
        }
        
        loaded = true;
    }
    
    public void scan() {
        if(!loaded) {
            throw new RuntimeException("Calling WebPage.scan without loading the page before.");
        }
        
        /* at first create all the WebResource-s then scan it,
         * we take it in two steps in order to be able to handle redirect
         */
        List<HarEntry> entries = scanner.proxyServer.getHar().getLog().getEntries();
        Set<String> loadedResources = new HashSet<>();
        
        for(int i = 0; i < entries.size();) {
            HarEntry entry = entries.get(i);
            String currentResourceURL = entry.getRequest().getUrl();
            loadedResources.add(currentResourceURL);
            
            /* if this entry doesn't exist, add it,
             * otherwise remove it from the list */
            if(scanner.hasResource(currentResourceURL)) {
                entries.remove(i);
            }
            else {
                WebResource newRes;
                if(this.toString().equals(currentResourceURL)) {
                    newRes = this;
                }
                else if(scanner.scanQueue.contains(currentResourceURL)) {
                    newRes = scanner.scanQueue.remove(currentResourceURL);
                }
                else {
                    newRes = new WebResource(currentResourceURL, scanner);
                }
                scanner.addResource(currentResourceURL, newRes);
                i++;
            }
        }
        
        for(HarEntry entry : entries) {
            String currentResourceURL = entry.getRequest().getUrl();
            String location = scanner.getResource(currentResourceURL).checkResponse(entry);
            if(location != null) {
                loadedResources.remove(location);
                scanner.getResource(location).addReference(
                        scanner.getResource(currentResourceURL));
            }
        }
        
        for(String url: loadedResources) {
            scanner.getResource(url).addReference(this);
        }
        
        /* check for javascript errors */
        for(JavaScriptError jsError: JavaScriptError.readErrors(driver)) {
            ErrorItem jsErrorItem = new ErrorItem("JS Error: "
                    + jsError.getErrorMessage(), Level.SEVERE,
                    jsError.getLineNumber(), ErrorItem.UNDEFINED);
            
            String resourceUrl = jsError.getSourceName();
            
            // if resourceUrl is from this site or reportAllErrors is yes, log this error
            boolean reportErr = true;
            try {
                boolean allErrorsConf =
                        WebPage.as.getProperty("reportAllErrors").equalsIgnoreCase("yes");
                boolean sameHost = scanner.getRootURL().getHost().equals(new URI(resourceUrl).getHost());
                reportErr = sameHost || allErrorsConf;
            }
            catch(URISyntaxException e) {}
            
            // resourceUrl exists (it was added in some step above)
            if(reportErr)
                scanner.getResource(resourceUrl).add(jsErrorItem);
        }
    }
    
    /**
     * Find all visible links on the page which points to some web page
     * (e.g. all relative links and absolute http and https, not mailto, etc.)
     * If any link has a suspicious href link (like unknown scheme name), add
     * it to the error log.
     * 
     * @return Set of absolute links.
     */
    Set<URI> findLinks() {
        if(!loaded) {
            throw new RuntimeException("Calling WebPage.findLinks without loading the page before.");
        }
        
        List<WebElement> anchorList = driver.findElementsByTagName("a");
        Set<URI> foundLinks = new HashSet<>();
        
        for(WebElement anchor: anchorList) {
            String href = anchor.getAttribute("href");
            if(href == null || href.isEmpty()) {
                /* TODO consider emitting a warning or info
                String html = driver.executeScript("return arguments[0].outerHTML;", anchor).toString();
                add(new ErrorItem("<a> tag with empty href (" + html + ").",
                        Level.INFO)); */
                continue;
            }
            
            try {
                URI hrefUri = new URI(href);
                String scheme = hrefUri.getScheme();
                if(scheme.equals("http") || scheme.equals("https")) {
                    if(scanner.isValidURL(href)) {
                        foundLinks.add(hrefUri);
                    }
                    else {
                        add(new ErrorItem("<a> tag with invalid href attribute ("
                                + href + ")", Level.SEVERE));
                    }
                }
                else if(!scheme.equals("mailto") &&
                        !scheme.equals("javascript") &&
                        !scheme.equals("ftp")) {
                    // log unknow scheme error
                    add(String.format("<a> tag with unrecognized"
                            + " scheme name (href=\"%s\")", href));
                }
            }
            catch(URISyntaxException e) {
                add(new ErrorItem("Cannot parse URI in <a href=\""
                        + href + "\"", Level.SEVERE));
            }
        }
        
        return foundLinks;
    }
}
