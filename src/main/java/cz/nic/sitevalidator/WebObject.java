/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.nic.sitevalidator;

import cz.nic.sitevalidator.util.ErrorItem;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author palko
 */
public class WebObject implements Iterable<ErrorItem> {
    private List<ErrorItem> errors = new ArrayList<>();
    protected final URL url;
    protected final SiteChecker scanner;
    
    public WebObject(String url, SiteChecker scanner) {
        this.scanner = scanner;
        try {
            this.url = new URL(url);
        }
        catch(MalformedURLException e) {
            throw new RuntimeException("Cannot create url from " + url, e);
        }
    }
    
    public WebObject(URL url, SiteChecker scanner) {
        this.scanner = scanner;
        this.url = url;
    }
    
    public boolean hasErrors() {
        /**
         * Return true/false if there are some errors to report.
         */
        return !errors.isEmpty();
    }
    
    /**
     * Add an ErrorItem to this WebObject.
     * 
     * @param error An error to add.
     */
    public void add(ErrorItem error) {
        if(error != null)
            errors.add(error);
    }
    
    /**
     * Convenient method to add error when the position is undefined (no line or
     * column is specified) and with severity WARNING.
     * 
     * @param msg error message
     */
    public void add(String msg) {
        if(msg != null)
            errors.add(new ErrorItem(msg));
    }
    
    public void add(Iterable<? extends ErrorItem> errorList) {
        for(ErrorItem err: errorList) {
            add(err);
        }
    }
    
    public int size() {
        return errors.size();
    }
    
    @Override
    public Iterator<ErrorItem> iterator() {
        return errors.iterator();
    }
    
    @Override
    public String toString() {
        return url.toString();
    }
    
    /** Two WebObjects are equal if and only if they point to the same url */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        
        if (obj == null || obj.getClass() != this.getClass()) {
             return false;
        }
        
        WebObject wobj = (WebObject) obj;
        return this.url.toString().equals(wobj.url.toString());
    }

    /** Returns hash code for this WebObject. Since this number is computed from
     final field, it is guaranteed not to change over time */
    @Override
    public int hashCode() {
        return 499 + this.url.hashCode();
    }
}
