package cz.nic.sitevalidator;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.logging.LogManager;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author palko <pavol.otto@nic.cz>
 */
public class ScanSite {
    private static final String usage = 
            "Usage: sitescan [-v|-s] [-c|-n] [-t sec] [-q|-a] url\n" +
            "       sitescan -h";
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String urlarg = null;
        
        /* parse command line arguments and set parameters to ApplSetting */
        HashMap<String, String> options = new HashMap<>();
        for(int i = 0; i < args.length; i++) {
            switch(args[i]) {
                case "-h":
                    try {
                        // we can copy it directly since it is a ASCII coded page
                        IOUtils.copy(
                                ScanSite.class.getResourceAsStream("/README"),
                                System.out);
                        System.exit(0);
                    }
                    catch(NullPointerException | IOException e) {
                        System.err.println("Error opening README file,"
                                + " search for help on our webpage.");
                        System.exit(1);
                    }
                case "-v":
                    options.put("reportAllErrors", "yes");
                    break;
                case "-s":
                    options.put("reportAllErrors", "no");
                    break;
                case "-c":
                    options.put("validateContent", "yes");
                    break;   
                case "-n":
                    options.put("validateContent", "no");
                    break;
                case "-t":
                    break;
                default:
                    // if -t sec, put it to ApplSettings "loadWait", it is validated
                    if(i != 0 && args[i - 1].equals("-t")) {
                        options.put("loadWait", args[i]);
                    }
                    else if (i + 1 == args.length) {
                        urlarg = args[i];
                    }
                    else {
                        System.err.println(String.format("invalid option '%s'", args[i]));
                        System.err.println(usage);
                        System.exit(2);
                    }
                    break;
            }
        }
        
        if(urlarg == null) {
            System.err.println(usage);
            System.exit(2);
        }
        
        // load ApplSettings and set parsed command line args
        try {
            ApplSettings.setCommandLineArgs(options);
        }
        catch(RuntimeException e) {
            System.err.println("Error creating ApplSettings. " + e.toString());
            System.exit(1);
        }
        
        // load logging.properties config file
        try
        {
            final InputStream inputStream =
                    ScanSite.class.getResourceAsStream("logging.properties");
            LogManager.getLogManager().readConfiguration(inputStream);
        }
        catch(NullPointerException | IOException e)
        {
            System.err.println("Could not load default logging.properties file.");
            System.exit(1);
        }
        
        try(SiteChecker sc = new SiteChecker(urlarg)) {
            sc.scanSite();
            sc.reportErrors();
        }
        catch(MalformedURLException e) {
            System.err.println("Malformed url " + urlarg);
            System.err.println("\n" + usage);
            System.exit(2);
        }
        catch(IOException e) {
            System.err.println("An IOException has occured " +
                    e.getClass().getName() + " " + e.getMessage());
            System.exit(2);
        }
        catch(Exception e) {
            System.err.println("An error occurred during validation: "
                    + e.getClass().getName() + " " + e.getMessage());
            System.exit(1);
        }
    }
}
