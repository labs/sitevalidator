package cz.nic.sitevalidator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.jsourcerer.webdriver.jserrorcollector.JavaScriptError;
import net.lightbody.bmp.proxy.ProxyServer;
import org.apache.commons.validator.routines.UrlValidator;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.w3c.css.util.ApplContext;
import org.w3c.css.util.CssVersion;

/**
 * @author palko <pavol.otto@nic.cz>
 */
class ProxyException extends IOException {
    public ProxyException(String msg) {
        super(msg);
    }
}

public class SiteChecker implements AutoCloseable {
    final ProxyServer proxyServer;
    private final FirefoxDriver driver;
    private WebPage driverLock = null;
    
    private final HashMap<String, WebResource> siteResources = new HashMap<>();
    
    private URL rootUrl;
    private static final Logger log
            = Logger.getLogger(SiteChecker.class.getCanonicalName());
    private static final ApplContext ac;
    private UrlValidator urlValidator;
    
    static {
        /* create and set up application context - do it here since it is same for all instances */
        ac = new ApplContext("en");
        ac.setCssVersionAndProfile(CssVersion.getDefault().toString()); // css3
        ac.setMedium("all");
        // vendorExtensionsAsWarnings:
        // false - report properties starting with - (like -moz-*, -webkit-*,...)
        // as errors; true - report them as warnings
        // (and the behaviour now is to discard the warnings)
        ac.setTreatVendorExtensionsAsWarnings(true);
        ac.setFollowlinks(false); // don't follow @import in css
    }
    
    class ScanQueue {
        private HashSet<String> addresses = new HashSet<>();
        private LinkedList<WebPage> resources = new LinkedList<>();
        
        /**
         * Appends the specified url to the end of this list unless
         * it has been already added.
         * 
         * @param url url of the page
         */
        public boolean add(WebPage newResource) {
            if(addresses.add(newResource.toString())) {
                return resources.add(newResource);
            }
            else {
                // add newResource.referencingResources to the existing
                WebPage res = resources.get(resources.indexOf(newResource));
                res.referencingResources.addAll(newResource.referencingResources);
                return false;
            }
        }
        
        public void addConditionally(String url, WebPage page) {
            // add to queue only if it hasn't already been scanned
            if(siteResources.containsKey(url)) {
                siteResources.get(url).addReference(page);
            }
            else {
                WebPage newPage = new WebPage(url, SiteChecker.this);
                newPage.addReference(page);
                add(newPage);
            }
        }
        
        
        public WebPage poll() {
            WebPage poll = resources.poll();
            if(poll != null)
                addresses.remove(poll.toString());
            return poll;
        }
        
        public boolean contains(String url) {
            return addresses.contains(url);
        }
        
        public WebResource remove(String url) {
            if(addresses.remove(url)) {
                Iterator<WebPage> iterator = resources.iterator();
                while(iterator.hasNext()) {
                   WebPage page = iterator.next();
                   if(page.toString().equals(url)) {
                       iterator.remove();
                       return page;
                   }
                }
                return null; // this will never happen, 
            }
            else {
                throw new NoSuchElementException();
            }
        }
    }
    
    ScanQueue scanQueue = new ScanQueue();
    
    public SiteChecker(String mainUrl) throws IOException {
        /* create urlValidator - if input address is localhost,... allow local addresses */
        UrlValidator onlyGlobalValidator =
                new UrlValidator(new String[]{"http", "https"});
        if (onlyGlobalValidator.isValid(mainUrl)) {
            urlValidator = onlyGlobalValidator;
        }
        else {
            UrlValidator withLocalValidator =
                    new UrlValidator(new String[]{"http", "https"},
                                     UrlValidator.ALLOW_LOCAL_URLS);
            if(withLocalValidator.isValid(mainUrl)) {
                urlValidator = withLocalValidator;
            }
            else {
                throw new MalformedURLException();
            }
        }
        
        rootUrl = new URL(mainUrl); // mainUrl has been validated, cannot throw a MalformedURLException
        // cut query and fragment, set path to "/" if not set
        rootUrl = new URL(rootUrl.getProtocol(),
                rootUrl.getHost(), rootUrl.getPort(),
                rootUrl.getPath().isEmpty() ? "/" : rootUrl.getPath());
        log.log(Level.FINE, "Page URL \"{0}\"", rootUrl.toString());
        
        /* start the proxy, find a first free port starting from 9000 */
        proxyServer = new ProxyServer();
        boolean running = false;
        for(int port = 9000; port < 9050; port++) {
            proxyServer.setPort(port);
            try {
                proxyServer.start();
                log.log(Level.FINE, "Starting proxy server on port {0}.", port);
                running = true;
                break;
            }
            catch(Exception e) {}
        }
        if(!running) {
            throw new ProxyException("Cannot create proxy server.");
        }
        
        // load /etc/hosts
        try (BufferedReader hosts = new BufferedReader(
                new FileReader("/etc/hosts"))) {
            String line;
            String[] parts;
            int nsindex;
            while((line = hosts.readLine()) != null) {
                if((nsindex = line.indexOf('#')) != -1) {
                    line = line.substring(0, nsindex);
                }
                line = line.trim();
                if(!line.isEmpty()) {
                    parts = line.split("\\s+");
                    if(parts.length == 2)
                        proxyServer.remapHost(parts[1], parts[0]);
                }
            }
        }
        catch(IOException e) {
            log.log(Level.WARNING,
                    "Error when loading /etc/hosts ({0})", e.getMessage());
        }
        
        Proxy selenium_proxy = null;
        try {
            selenium_proxy = proxyServer.seleniumProxy();
        }
        catch(UnknownHostException e) {
            try {
                proxyServer.stop();
            }
            finally {
                throw new ProxyException("Cannot create selenium proxy object"
                                         + " (" + e.getMessage() + ").");
            }
        }
        
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.PROXY, selenium_proxy);
        
        FirefoxProfile ffProfile = new FirefoxProfile();
        JavaScriptError.addExtension(ffProfile);
        
        proxyServer.setCaptureContent(true);
        proxyServer.setCaptureBinaryContent(false);
        proxyServer.setCaptureHeaders(true);
        try {
            driver = new FirefoxDriver(new FirefoxBinary(),
                                       ffProfile, capabilities);
            log.fine("Browser started.");
        }
        catch(RuntimeException e) {
            try{
                proxyServer.stop();
            }
            finally {
                throw new RuntimeException("Cannot create selenium proxy object"
                                           + " (" + e.getMessage() + ").");
            }
        }
    }
    
    @Override
    public void close() {
        try {
            proxyServer.stop();
            log.fine("Proxy server shut down.");
        }
        catch(Exception e) {
            log.log(Level.WARNING, "Cannot shut down proxy server ({0})",
                    e.getMessage());
        }
        
        try {
            driver.close();
            log.fine("Browser closed.");
        }
        catch(Exception e) {
            log.log(Level.WARNING,"Cannot shut down webdriver browser "
                    + "instance({0})", e.getMessage());
        }
    }
    
    boolean isSubPage(URI url) {
        return (url.getHost().equalsIgnoreCase(rootUrl.getHost())
                && url.getPath().startsWith(rootUrl.getPath()));
    }
    
    boolean isSubPage(URL url) {
        return (url.getHost().equalsIgnoreCase(rootUrl.getHost())
                && url.getPath().startsWith(rootUrl.getPath()));
    }
    
    boolean isSubPage(String url) throws MalformedURLException {
        return isSubPage(new URL(url));
    }
    
    /**
     * Returns true if given resource exists.
     * @param url Url of the resource
     */
    public boolean hasResource(String url) {
        return siteResources.containsKey(url);
    }
    
    /**
     * Add WebResource to this site unless it already exists.
     * The resource is identified by its URL.
     * @param url
     * @param resource
     * @return Returns true/false if it was added.
     */
    public void addResource(String url, WebResource resource) {
        boolean addResource = !siteResources.containsKey(url);
        if(addResource) {
            siteResources.put(url, resource);
        }
    }
    
    public WebResource getResource(String url) {
        return siteResources.get(url);
    }
    
    public void scanSite() {
        scanQueue.add(new WebPage(rootUrl, this));
        WebPage scannedPage;
        
        /* while there are pages in the scanQueue heap, take one and scan */
        while((scannedPage = scanQueue.poll()) != null) {
            log.log(Level.FINE, "Scanning {0}", scannedPage.toString());
            
            /* decide what to do, if use selenium to validate */
            boolean isSubPage;
            try {
                isSubPage = isSubPage(scannedPage.toString());
            }
            catch(MalformedURLException e) {
                log.log(Level.SEVERE, "Internal error: Skipping page {0}."
                        + "Cannot parse URL.", scannedPage.toString());
                continue; // this cannot happen, urlPage is always valid
            }
            
            // here we do HEAD request to check the resource and only if returned
            // html or css and is subpage of main page, continue with selenium
            // HEAD request to check for existence
            String headCheckAddr = scannedPage.headCheck();
            
            
            // headCheck null denotes we should use selenium
            if(!isSubPage || headCheckAddr != null) {
                // we won't use selenium, add scannedResource to list
                addResource(scannedPage.toString(), scannedPage);
                
                // handle redirect
                if(headCheckAddr != null && !headCheckAddr.isEmpty()) {
                    scanQueue.addConditionally(headCheckAddr, scannedPage);
                }
                
                continue;
            }
            
            /* start of validation by selenium */
            scannedPage.load();
            // search for links (<a href="...">)
            for(URI link: scannedPage.findLinks()) {
                /* cut the fragment part (after #)
                 * and check that it is not this page's address */
                if(link.getFragment() != null) {
                    try {
                        link = new URI(link.getScheme(), link.getUserInfo(),
                                link.getHost(), link.getPort(),
                                link.getPath(), link.getQuery(), null);
                    }
                    catch(URISyntaxException e) {} // this exception cannot occur
                }
                
                // filter out the link to scanned page itself
                if(!link.toASCIIString().equals(scannedPage.toString())) {
                    scanQueue.addConditionally(link.toASCIIString(), scannedPage);
                }
                // TODO else if link to itself, check if id="$fragment" exists
            }
            scannedPage.scan();
            
            freeFirefoxLock(scannedPage);
        }
    }
    
    public void reportErrors() {
        System.out.println();
        System.out.println("-------------------------");
        System.out.println("Errors found on the site:");
        boolean noErrors = true;
        
        for(WebResource resource : siteResources.values()) {
            if(resource.hasErrors()) {
                noErrors = false;
                System.out.println(resource.reportLinkingResources());
                System.out.println(resource.reportErrors());
                System.out.println("-------------------------");
            }
        }
        
        if(noErrors) {
            System.out.println("No errors were found on the site.");
            System.out.println("-------------------------");
        }
    }
    
    /**
     * Obtain lock signifying that this page is using Firefox.
     * @return Firefox webdriver
     */
    public FirefoxDriver getFirefox(WebPage page) {
        // if webpage null or same as already set, return driver, else throw an exception
        if(driverLock == null) {
            driverLock = page;
        }
        else if(driverLock != page) {
            throw new RuntimeException("cannot obtain lock for WebDriver, webpage "
                    + driverLock.toString() + " has not feed it yet");
        }
        
        return driver;
    }
    
    /**
     * Returns if page page already has the lock for Firefox.
     * 
     * @param page
     */
    public boolean hasLock(WebPage page) {
        return driverLock == page;
    }
    
    /**
     * Free the lock for Firefox instance
     * @param page WebPage that holds the lock
     */
    public void freeFirefoxLock(WebPage page) {
        if(driverLock.equals(page)) {
            driverLock = null;
            page.loaded = false;
        }
        else {
            throw new RuntimeException("WebPage " + page.toString()
                    + " is trying to free the lock that it doesn't hold.");
        }
    }
    
    /**
     * Return the url of the scanned site.
     * 
     * @return root url
     */
    public URL getRootURL() {
        return rootUrl;
    }
    
    public boolean isValidURL(String checkUrl) {
        return urlValidator.isValid(checkUrl);
    }
}
