#!/bin/sh

sourcedir=`dirname "$0"`

# check if install dir specified
if [ -z "$1" ]; then
    echo "install dir not specified" >&2
    exit 1
fi

# check if install dir exists and is empty, (create it if doesn't exist)
if [ ! -e "$1" ]; then
    if ! mkdir -p "$1"; then
        echo "cannot create directory ‘$1’" >&2
        exit 1
    fi
elif [ ! -d "$1" ]; then
    echo "‘$1’ is not a directory" >&2
    exit 1
fi

dl=`ls -A "$1"`
if [ -n "$dl" ]; then
    echo "directory ‘$1’ is not empty"
    exit 1
fi

# compile and fetch dependencies
cd "$sourcedir"
mvn install
cd -

# we have empty install directory, copy files
echo "INSTALLING..."
jarfile=`echo "$sourcedir"/target/*.jar`
jarfname=`basename "$jarfile"`
mv "$sourcedir/target/lib" "$1"
mv "$jarfile" "$1" # jar name can vary across versions
cp "${sourcedir}/sitevalidator" "$1"
chmod a+x "$1/sitevalidator"


# copy user-config file
echo "COPYING CONFIG FILE..."
cf=~/.java/sitevalidator.xml
cfdir=`dirname "$cf"`
if [ -f "$cf" ]; then
    echo "file ‘$cf’ already exists, copy sitevalidator.xml to ‘$cf’ manually"
elif ! ( mkdir "$cfdir"; cp "$sourcedir/sitevalidator.xml" "$cf" ) then
    echo "couldn't copy config file, copy sitevalidator.xml to ‘$cf’ manually"
else
    echo "SUCCESSFULLY INSTALLED"
    echo
    echo "Now you SHOULD edit confing file ‘$cf’ See INSTALL for more details."
fi
